
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳
.. 🇫🇷
.. 🇧🇪
.. 🇨🇭
.. 🇩🇪
.. 🌍 ♀️
.. 🇮🇷



|FluxWeb| `RSS <https://femme-vie-liberte.frama.io/expositions-2023/grenoble/rss.xml>`_

.. _exposition_iran:
.. _iran_exposition:

=======================================================================================================================================================
|ForoughFarrokhzad| **Exposition Femme Vie Liberté** du lundi 7 mars au vendredi 31 mars 2023 à la Maison de l’International à Grenoble |JinaAmini|
=======================================================================================================================================================

- http://iran.frama.io/linkertree

.. figure:: images/logo_exposition.png
   :align: center
   :width: 300


L'exposition **Femme Vie Liberté** se donne comme objectif de mettre en
lumière la lutte des femmes iraniennes.
Elle décrira la répression en Iran depuis la révolution de 1979 et
tentera de retracer la révolution en cours depuis le meurtre de Mahsa
Jina Amini par le gouvernement iranien.

L'exposition comprend également une partie artistique en relation avec
**Femme Vie Liberté**, exposant des œuvres de peinture et de céramique
produites par des artistes iraniennes grenobloises.


.. figure:: images/mahsa_amini.png
   :align: center


.. figure:: images/A3_expo_femme_vie_liberte.png
   :align: center
   :width: 500

   https://www.grenoble.fr/98-maison-de-l-international.htm, https://www.openstreetmap.org/#map=19/45.19225/5.72776


.. raw:: html

   <iframe width="600" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
   src="https://www.openstreetmap.org/export/embed.html?bbox=5.725188553333283%2C45.1914775748417%2C5.729667842388153%2C45.193027566362716&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small>
   <a href="https://www.openstreetmap.org/#map=19/45.19225/5.72743">Afficher une carte plus grande</a></small>

::

    Maison de l'International
    1, rue Hector Berlioz
    Jardin de ville
    38000 Grenoble

    parvis des droits de l'Homme Jardin de Ville


.. figure:: images/entree_maison_internationale.png
   :align: center
   :width: 300

   Entrée de la maison de l'International


.. figure:: images/entree_maison_internationale_2.png
   :align: center
   :width: 300

Le Collectif Iran Solidarités et la Ligue pour la défense des droits de
l'Homme en Iran vous proposent cette exposition consacrée à L'Iran
**Femme, Vie, Liberté**, trois mots qui secouent le monde depuis plusieurs mois,
trois mots qui sont devenus l'emblème d'une révolution en marche en Iran.

Depuis la mort de Mahsa Jina Amini, ce slogan est scandé dans toutes les rues en
Iran pour réclamer les droits fondamentaux des femmes et de toute La population

Réprimé dans le sang depuis 43 ans, le peuple iranien est déterminé à reprendre
son destin en main.


.. toctree::
   :maxdepth: 6

   salles/salles
   03/03
   videos/videos
   ressources/ressources
