
.. index::
   pair: Salle 1 (histoire du féminisme en Iran) ; Grenoble
   pair: Tâhereh; poétesse
   pair: Tâhereh; pionniere du mouvement féministe en Iran
   pair: Tâhereh; Fāṭemeh
   pair: Fāṭemeh; Féminisme
   pair: Fāṭemeh; Tâhereh
   pair: Histoire; Féminisme
   pair: Chanteuse ; Qamar-ol-Moluk Vaziri
   pair: Qamar-ol-Moluk ; Vaziri

.. _salle_1_grenoble:

=========================================================================================================
**Salle 1 de l'exposition, histoire du féminisme en Iran**
=========================================================================================================


.. _histoire_feminisme:

Histoire du féminisme en Iran
=================================

.. figure:: images/panneau_histoire.jpeg
   :align: center



.. figure:: images/image_3_2_1_histoire.jpeg
   :align: center


.. figure:: images/panneau_2_1_histoire.jpeg
   :align: center


.. _trois_periodes:

**Trois périodes Eveil, Education, Journalisme**
===================================================

.. figure:: images/panneau_1.jpeg
   :align: center



.. _eveil_des_femmes:

**1) L'éveil des femmes**
============================


.. _fatameh_tahereh:

Fāṭemeh (1817-1818 et morte à Téhéran en 1852) (persan : فاطمه) également aussi connue sous le nom de Tâhereh (« La Pure »), Qurratu’l-‘Ayn (arabe : قرّة العين « Consolation des Yeux »)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://fr.wikipedia.org/wiki/Fatemeh
- https://fr.wikipedia.org/wiki/Babisme

Fāṭemeh (persan : فاطمه), de son vrai nom Fāṭimih Baraghāní, née à Qazvin
vers 1817-1818 et morte à Téhéran en 1852, est un des premiers personnages
marquant de l'histoire des mouvements féministes en Iran.

Elle fut aussi une grande poétesse, et une théologienne de renom.
Elle est aussi, et peut-être surtout, la première femme disciple du `Bāb <https://fr.wikipedia.org/wiki/B%C4%81b_(chef_religieux)>`_


Elle est notamment célèbre pour avoir jeté son voile durant la conférence
de Badasht en 1848.


.. _bibi_khanoom:

Bibi Khanoom Astarabadi (1858/9–1921)
------------------------------------------

- https://fr.wikipedia.org/wiki/Bibi_Khanoom_Astarabadi

Bibi Khānoom Astarābādi (persan : بی بی خانوم استرآبادی)‎ (1858/9–1921) est une
célèbre écrivaine et satiriste iranienne.

Elle fut une pionnière du droit des femmes en Iran.

Elle est l'une des figures majeures de la révolution constitutionnelle
iranienne de la fin du XIXe siècle et du début du XXe.

Elle fonde en 1907 la première école pour filles de l'époque moderne
iranienne et écrit de nombreux articles pour les droits des femmes à
l'éducation.
Ses articles paraissent dans de nombreux journaux comme Tamaddon (تمدن - Civilisation),
Habl al-Matin (حبل المتين - Corde Solide) and Majles (مجلس - Parlement).

Elle est également connue pour son livre Ma'ayeb al-Rejal (معايب الرجال -
Echecs des Hommes), une réponse critique à un pamphlet anonyme
Ta'deeb al-Nesvan (تاديب النسوان - Education des Femmes).

Publié en 1895, onze ans avant l'inauguration par décret du système
monarchique constitutionnel iranien, le livre est considéré comme la
première déclaration pour les droits des femmes en Iran.


.. _education_des_femmes:

**2) L'éducation des femmes**
======================================

.. figure:: images/panneau_2_bis.jpeg
   :align: center


.. _toubi_azmodeh:

Toubi Azmoudeh
-----------------

- https://iranwire.com/en/influential-women/117986-iranian-influential-women-touba-azmoudeh-1878-1936/


.. _journalisme:

3) **Le journalisme**
===============================


.. figure:: images/panneau_3.jpeg
   :align: center


.. _sediqeh_dowlatabadi:

Sediqeh Dowlatabadi
----------------------

- https://fr.wikipedia.org/wiki/Sediqeh_Dowlatabadi

Sediqeh Dowlatabadi (1882, Ispahan – 30 juillet 1961) (persan : صدیقه دولت‌آبادی)
est une journaliste iranienne, militante féministe et pionnière du mouvement
perse pour les droits des femmes1.

Son père est un religieux libéral, chef de file de la communauté babie,
qui permet à ses filles de poursuivre des études.

Elle les complète à la Sorbonne. Elle maîtrise le français et l'anglais.

Elle dirige, à partir de 1919, le premier magazine féminin à Ispahan :
Zaban-e Zanan (« La Langue des femmes »).
La ligne éditoriale se concentre sur le droit des femmes à l'instruction
et critique les traditions patriarcales du pays.

Le journal est interdit sur ordre du premier ministre Tonekaboni.

Elle fonde une association féministe , qui doit déménager d'Ispahan à
Téhéran après une attaque par des fondamentalistes.
L'association se bat pour que les droits aux femmes soient reconnus, en
particulier en matière d'éducation, mais sans succès, puisque la nouvelle
constitution, en 1906, ne leur accorde pas le droit de vote et limite
leur droit à l'instruction.
Elle ouvre une école de filles en 1918, qui doit fermer après seulement
quelques mois.


.. _forough_farrokhzad:

|ForoughFarrokhzad| **Forough Farrokhzad** poétesse contemporaine iranienne
================================================================================


- https://fr.wikipedia.org/wiki/Forough_Farrokhzad


**Forough Farrokhzad** (en persan : فروغ فرخزاد), née le 5 janvier 1935 à Téhéran
et morte le 13 février 1967 dans la même ville, est une poétesse
contemporaine iranienne.

Née dans une famille de militaires à Téhéran en 1935, Forough épouse à
l’âge de 16 ans Parviz Shapour, satiriste iranien de renom, puis déménage
à Ahvaz pour suivre son mari avec lequel elle apprend la peinture.

C'est à partir de ce moment qu'elle commence à correspondre avec des
magazines de renom.

Son premier recueil de poésies, اسير (« Captive »), est publié en 1955.
On y ressent la large influence de Fereydoun Moshiri, Nader Naderpour
et Fereydoun Tavalalli.

Ses recueils suivants sont ديوار (« Le mur »), publié en 1956, et عصيان
(« La rébellion »), publié en 1958.
C'est au cours de cette même année qu'elle rencontre Ibrahim Golestan,
célèbre écrivain et cinéaste iranien, et qu'elle commence à coopérer avec lui.

Forough Farrokhzad poursuit des études cinématographiques en Angleterre
en 1959 puis joue dans un film intitulé La Proposition en 1960.

Forough retourne en Angleterre l'année suivante.

Elle déménage à Tabriz en 1962 et réalise son film La maison est noire
(en persan : خانه سیاه است, Kẖạneh sy̰ạh ạst), un film sur la vie des lépreux.
Le film remporte le Grand prix documentaire au Festival Oberhausen en 1963.

Elle joue la même année dans une pièce de Luigi Pirandello intitulée
Six personnages en quête d'auteur. Elle publie cette même année 1963
son recueil تولدى ديگر (« Une autre naissance ») qui représente en effet
une nouvelle naissance pour la poésie persane.

Forough visite l'Allemagne, la France et l'Italie en 1964.

Forough Farrokhzad décède le 14 février 1967 dans un accident de voiture.

Son dernier recueil de poèmes, intitulé ايمان بياوريم به اغاز فصل سرد (« Laissez-nous
croire au début de la saison froide »), est publié de manière posthume.

En décembre 2006, une traduction de sélection de ses poèmes en anglais,
faite par Maryam Dilmaghani, a été publiée en ligne pour célébrer le
quarantième anniversaire de son décès.



.. figure:: images/forough_farrokhzad_panneau.jpeg
   :align: center

.. figure:: images/forough_farrokhzad.jpeg
   :align: center


Poème
========

.. figure:: images/ecriture_persane.jpeg
   :align: center


Qamar Vazir et Shirin Ebadi
===============================

.. figure:: images/tahareh_zarin_taj.png
   :align: center


.. _qamar_vaziri:

**Qamar-ol-Moluk Vaziri, (1905-1959) chanteuse iranienne célèbre**
===========================================================================

- https://fr.wikipedia.org/wiki/Qamar-ol-Moluk_Vaziri

Qamar-ol-Molouk Vaziri (en persan: قمرالملوک وزیرى) appelée communément Qamar
(mot arabe pour la lune) est une chanteuse iranienne célèbre, née en
1905 et morte en 1959.
Elle était considérée comme la «reine» de la musique classique persane.

Elle est aussi la première chanteuse à oser abandonner le hijab pour se
produire tête nue face à un public d'hommes.

C'était une mezzo-soprano, vénérée pour sa maîtrise du répertoire de la
musique vocale persane (radif) et ses interprétations sensibles.

Son parcours
-------------------

Elle est née en 1905 à Takestan, en Iran. Sa mère est morte de fièvre
typhoïde alors qu'elle n'avait qu'un an et demi.
Elle a ensuite perdu son père, et sa grand-mère est devenue son tuteur
légal.

Cette grand-mère, Molla 'Khayr-ol-Nesa était une chanteuse reconnue,
mise à contribution, notamment, dans des cérémonies de la cour du
Nasseredin Shah, et dans des cérémonies religieuses évoquant le martyre
des chiites à la bataille de Kerbala.

Elle accompagne sa grand-mère dans ses différents récitals.

Elle chante et se produit à son tour. Très appréciée dans les années 1920,
**elle n'hésite pas à se produire tête nue devant les publics masculins,
à une époque où les mollah ne toléraient pas les femmes sans voile dans
la rue ou dans une assistance**.

Elle est menacée, insultée, mais elle tient bon.

Vazirizāda est un nom d'artiste qu'elle choisit en l'honneur d'un musicien,
et théoricien de la musique, 'Ali-Naqi Vaziri'.

Elle arrête de chanter en 1956 après 30 ans d’interprétations de chansons
et après avoir collaboré avec bien des auteurs-compositeurs et poètes
de l'époque.

Elle est morte en 1959 à Shemiran, Téhéran, et est enterrée au cimetière
Zahir o-dowleh.


Liens
--------

- :ref:`iran_2023:no_lands_son_2023_11_10`


.. _shirin_ebadi:

**Shirin Ebadi, Prix Nobel de la paix, militante politique iranienne, avocate, ancienne juge et militante des droits humains**
=================================================================================================================================

- https://fr.wikipedia.org/wiki/Shirin_Ebadi
- :ref:`shirin_Ebadi_2023_03_07`
- :ref:`chinsky:guerrieres_paix_2023_11_05`

Shirin Ebadi (en persan : شیرین عبادی, Shirin 'Ebādi), née le 21 juin 1947
à Hamadan en Iran, est une militante politique iranienne, avocate,
ancienne juge et militante des droits humains.

Le prix Nobel de la paix lui a été décerné en 2003 pour ses accomplissements
en faveur de la défense des droits humains, plus précisément de la
démocratie, des droits des femmes et des enfants.

En 2004, le magazine Forbes la place parmi les **100 femmes les plus
influentes dans le Monde**.

**Narges Mohammadi, prix Nobel de la Paix 2023**
-------------------------------------------------------

- :ref:`iran_2023:nobel_2023_10_06`
