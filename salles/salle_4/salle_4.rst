
.. index::
   pair: Salle 4 (exposition des œuvres d'artistes iraniennes); Grenoble
   pair: Artistes ; Salle 4 (Grenoble)

.. _salle_4_grenoble:

=========================================================================================================================
**Salle 4: exposition des œuvres d'artistes iraniennes en relation avec le thème Femme Vie Liberté**
=========================================================================================================================


**Damné**
============

.. figure:: images/tableau_homme_barbu.jpeg
   :align: center


Femme avec cheveux coupés
=============================

.. figure:: images/tableau_femme_avec_cheveux_coupes.jpeg
   :align: center


Quelques tableaux
=======================

.. figure:: images/vue_tableaux.jpeg
   :align: center

2 tableaux
=============

.. figure:: images/2_tableaux.jpeg
   :align: center

Jeunes filles sans voiles
============================

.. figure:: images/tableau_jeunes_filles.jpeg
   :align: center


Jina Mahsa Amini
==================

.. figure:: images/tableau_jina_mahsa_amini.jpeg
   :align: center


.. _persona_non_grata:

**Persona non grata** En Iran, je suis aussi persona non grata que les plus gros dealers d'héroïne
======================================================================================================

- :ref:`iran_2024:arrestation_ahou_daryaei_2024_11_02`

En Iran, je suis aussi persona non grata que les plus gros dealers d'héroïne.


.. figure:: images/tableau_persona_non_grata.jpeg
   :align: center


Petite fille dans la tourmente
================================

.. figure:: images/tableau_petite_fille_dans_la_tourmente.jpeg
   :align: center

Vieil homme avec oiseau
==========================

.. figure:: images/tableau_vieil_homme_avec_oiseau.jpeg
   :align: center

Tapis avec une femme
======================

.. figure:: images/tableau_x.jpeg
   :align: center


Vue sur la salle
=================

.. figure:: images/vue_salle.jpeg
   :align: center
