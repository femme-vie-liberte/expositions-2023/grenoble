
.. index::
   ! Tchaharchanbé-Souri

.. _tchaharchanbe_souri:

=============================================================================================
Mardi soir 14 mars 2023 **Tchaharchanbé-Souri ou la fête du feu** #chaharshanbehsoori
=============================================================================================


Définition wikipedia
======================

- https://fr.wikipedia.org/wiki/Norouz#Tchaharchanb%C3%A9-Souri


Le dernier mardi soir de l'année est célébré par les Iraniens sous le nom
de Tchaharchanbé-Souri (en persan : چهارشنبه‌سوری), moment où tout le monde
sort dans la rue, fait des feux et saute par-dessus en criant::

    « Zardi-yé man az to ; sorkhi-yé to az man »

(en persan : زردی من از تو، سرخی تو از من) qui signifie littéralement : « ma [couleur]
jaune pour toi, ta [couleur] rouge pour moi » (le rouge est la couleur du feu),
c'est-à-dire, figurativement, « je te donne ma pâleur — ou ma maladie —,
je prends ta force — ta santé ».

Offrir des pâtisseries connues sous le nom de Ajile Moshkel Gosha est la
façon de remercier pour la santé et le bonheur de l'année passée, tout
en échangeant toute pâleur et tout mal restant pour la chaleur et les
vibrations du feu.

D'après la tradition, les esprits des ancêtres rendent visite aux vivants
les derniers jours de l'année, et beaucoup d'enfants s'entourent de draps,
rejouant ainsi symboliquement les visites des morts.

Ils courent aussi dans les rues en tapant sur des boîtes et des casseroles
et frappent aux portes pour jouer des tours aux gens.

Ce rituel est appelé **qashogh-zany** (battage de cuillers) et symbolise
le fait de chasser le dernier mardi de malchance de l'année.

La tradition veut également que l'on saute dans l'eau le mercredi matin
aux premiers rayons de soleil.

Il y a plusieurs autres traditions cette nuit-là, dont les rituels de
Kouzéh Chékastan, pendant lequel on casse des jarres en terre qui
contiennent symboliquement la mauvaise fortune de quelqu'un, Fâl-gouch
ou l'art de la divination en écoutant les conversations des passants et
le rituel de Géréh-gochâyi, faire un nœud dans un mouchoir ou un tissu
et demander au premier passant de le défaire afin d'éloigner la malchance
de quelqu'un.


Dessins de Bahareh Akrami #IranianRevolution #Iran
========================================================

Joyeux Tchaharchanbé-Souri   #IranianRevolution #Iran

- :ref:`akrami_iran:dessins_bahareh_akrami`

.. figure:: images/akrami.png
   :align: center

   https://nitter.nicfab.eu/Baboobabounette/status/1635686236198252568#m


Annonce d'actions en Iran
=============================

.. figure:: images/affiche.png
   :align: center

Tcharchanbé souri ou la fête du feu est une fête pré-islamique que les
Iraniens célèbrent depuis toujours, le dernier mardi soir de l’année iranienne.

cette année c’est ce mardi 14 mars, l’occasion pour les Iraniens de dire
non au régime islamique

Un appel à la mobilisation est lancé.



- https://twitter.com/LettresTeheran/status/1635220663735828481#m

.. figure:: images/bruler_les_voiles.png
   :align: center

   Graffiti en Iran : « Tcharchanbé souri (la fête du feu, ce mardi), nous brûlerons les voiles »


Cette fois, le prétexte est #chaharshanbehsoori, la tradition ancienne
du "mercredi du feu" mais ne vous y trompez pas, dans ces appels à la rue
aujourd'hui demain et jeudi, c'est toujours la même rage qui brûle:

#mahsaamini & #iranrevolution #betheirvoice #beourvoice #iranbond

