.. index::
   pair: Visite; Exposition
   pair: Eric Piolle; Visite Exposition

.. _eric_piolle_2023_03_08:

========================================================================
Mercredi 8 mars 2023 **Visite d'Eric Piolle**
========================================================================


Commentaire
=============


- https://singapore.unofficialbird.com/EricPiolle/status/1633525529381855238#m

La Maison de l’International de Grenoble expose les œuvres d'artistes
d'Irannien·nes, à l’heure où ce peuple se bat pour les droits humains
et celui des femmes à disposer de leur corps.


Salle 2
=======

- :ref:`salle_2_grenoble`

.. figure:: images/salle_2.png
   :align: center


Salle 3
=======

- :ref:`salle_3_grenoble`


.. figure:: images/salle_3.png
   :align: center


Salle 4
=======

- :ref:`salle_4_grenoble`

.. figure:: images/salle_4.png
   :align: center
