.. index::
   ! Nowruz
   ! Norouz


.. _norouz_2023_03_20:

===============================================================================================================================
Lundi 20 mars 2023 22h24 **Norouz (en persan: نوروز nowruz)**
===============================================================================================================================

.. figure:: images/affiche_nowruz.png
   :align: center


Nom officiel
=============

- https://fr.wikipedia.org/wiki/Norouz

Norouz, Norooz, Narooz, Nawruz, Newroz, Newruz, Nauruz, Nawroz, Noruz,
Novruz, Nauroz, Navroz, Naw-Rúz, Nowroj, Navroj, Nevruz, Navruz,
Navrez, Nooruz, Nauryz, Nevruz, Nowrouz, Nezrouz


Nom de la fête
================

- https://fr.wikipedia.org/wiki/Norouz

Le mot vient de l'avestique nava, « nouveau » + rəzaŋh, « jour »/« lumière du jour »
(« nouveau jour »/« nouvelle lumière »), et qui a toujours le même sens
en persan (no, « nouveau » + rouz, « jour », signifiant « nouveau jour »)


Définition Wikipedia
=======================

- https://fr.wikipedia.org/wiki/Norouz

Norouz (en persan: نوروز nowruz) est la fête traditionnelle des peuples
iraniens qui célèbrent le nouvel an du calendrier persan (premier jour
du printemps).

La fête est célébrée par certaines communautés le 21 mars et par d'autres
le jour de l'équinoxe vernal, dont la date varie entre le 20 et le 22 mars.

Norouz a des origines iraniennes et zoroastriennes ; cependant, depuis
plus de 3000 ans, cette fête est célébrée par diverses communautés en
Asie de l'Ouest, Asie centrale, Caucase, bassin de la Mer Noire, Balkans
et Asie du sud.

C'est une fête culturelle et religieuse (voir Zoroastrianisme et Baha'i)

En français, Norouz est également appelé Nouvel An iranien ou Nouvel An
persan.

Le Norouz est inscrit à l'inventaire du patrimoine culturel immatériel
en France en 2019.

Article du point
===================

- https://www.lepoint.fr/art-de-vivre/fete-printaniere-de-norouz-le-mystere-des-sept-elements-29-03-2020-2369260_4.php

En France, on l'appelle « Norouz », fête de la nouvelle année, célébrée
à partir de l'équinoxe de printemps, par près de 300 millions de personnes
dans monde : en Iran, berceau du zoroastrisme, qui en perpétue la tradition,
et dans d'autres pays ayant connu l'influence de l'Empire perse sassanide,
comme l'Azerbaïdjan, l'Inde, le Kirghizistan, le Pakistan, la Turquie,
l'Ouzbékistan, le Tadjikistan.

**Cette célébration est calée sur le calendrier solaire**, qui tranche avec
le calendrier lunaire musulman et est un des symboles importants de la
spécificité du monde iranien, remontant à l'Antiquité.

Selon la tradition, Norouz perpétue et commémore le jour de la création
du monde par Ahura Mazda.

Durant treize jours, chaque foyer entretient soigneusement une table
dressée d'au moins sept objets symboliques, des objets aux signifiés
variables selon la religion et/ou la région.

Sa configuration – c'est-à-dire l'espace où figurent les objets – n'est
pas immuable et sa mise en scène ne suit pas un plan prédéfini, comme
nous avons pu l'observer, lors d'une soirée organisée par une association
iranienne de l'Île-de-France.

« Tout est dans la tête », dit Alina. Chaque année, elle et d'autres
femmes de l'association cherchent à apporter une nouvelle touche, avec
des éléments de décoration (motifs) ou en jouant sur les dimensions de
la table.

Cette table n'est pas une structure figée, mais un continuum de glissements,
adjonctions de choses, abandons de l'autre.

Le travail de mise en scène commence par la pose d'une nappe rituelle –
dont la taille n'est pas définie par la dimension de la table, puisque
celle-ci change – sur un support en bois, surmonté de deux caisses
ajourées en plastique.

Yasmine apporte un récipient en plastique qu'elle pose sur la structure,
puis le recouvre d'un drap blanc, afin de caler le miroir.
Mais, cela ne lui convient pas. Elle enlève le drap blanc, le seau en
plastique et étale un tissu de velours jaune sur le drap blanc.

« On fait des essais », dit-elle. Sur les deux couches de tissu est étendu
un tissu plus épais, avec des motifs traditionnels iraniens, inspirés de
la grenade.
Les draps ne sont pas tendus, il faut que ça flotte, qu'il y ait du mouvement.
Pendant ce temps, Alina prépare les drapeaux. Yasmine sort un motif
qu'elle a transposé sur une feuille blanche par décalcomanie.

Une chaise est apportée puis recouverte par le tissu de velours jaune.
Mise en place du miroir.

Une fois le miroir posé, un sabzeh, germe de blé ou lentille poussant
dans un plat (symbole de la renaissance), est disposé devant.

Yasmine cherche un emplacement pour le livre de poésie. En l'ouvrant,
elle découvre à l'intérieur des billets de 10 euros.
Elle raconte que, lors de la soirée de Norouz, les parents lisent un poème,
insèrent un billet, referment le livre et l'offrent aux enfants.

Un beau travail de composition
===================================


Essai de positionnement du motif. Mise en place de l'aquarium, d'une coupe en verre.

D'autres éléments sont rassemblés sur le côté droit de la table : bougies,
qui, allumées, deviennent le signe d'une pureté d'intention de cœur,
fleur de pensée (la bonne pensée),
samanu – crème très sucrée faite avec des germes de blé (symbole de l'abondance),
sekkeh – pièces de monnaie (symbole de la prospérité et de la fortune),
objet de décoration bleu,
mini-sabzeh (à base de germes de lentilles) dans une coquille d'œuf,
sonbol- jacinthe (symbole de l'arrivée du printemps).

Yasmine enrobe les pommes d'huile et les frotte avec un tissu pour ôter
l'excédent.

Mise en place du deuxième gros sabzeh (de blé).

Les coquilles d'œuf à l'intérieur desquelles a poussé le sabzeh sont
posées sur un minuscule contenant, de type dé à coudre, qui contient
de la limaille d'étain. Les œufs colorés et décorés de motifs var

Les œufs colorés et décorés de motifs variés sont disposés autour du sabzeh
de blé de façon à former une couronne.

Les éléments, senjed – fruit séché du jujubier (symbole de l'amour),
somok – baies de sumac, épice antioxydante (symbole de la couleur du
lever du soleil et santé), bonbons, stockés dans de petits bols individuels
de verre, sont répartis sur des coupelles de couleur bleue.

Le sir – ail (symbole de la médecine) est posé sur la coupelle puis
épluché.

Yasmine prépare le motif : après avoir appliqué de la colle, elle parsème
le motif de granulés rouges, appelés esphan.

« On le brûle et ça porte bonheur. » Les biscuits sont également placés
sur une coupelle.
Yasmine interrompt son travail et regarde la cohérence de l'ensemble.
Encore quelques ajustements : elle déplace le pot de pensées, le place
à droite, puis à gauche. Elle répète ce geste pour la coupelle de senjed,
celle de bonbons et l'ail.
Il manque quelque chose.
Elle a oublié le serkeh – vinaigre (symbole de l'âge et la patience).

Encore quelques ajustages. Au fil de l'évolution de la table, on perçoit
la motivation à réaliser un beau travail de composition – comme on le
ferait pour une composition florale, leur fierté à nous montrer leur
chef-d'œuvre.
Il n'y a pas de schémas, mais il faut que, à la fin, ce soit harmonieux,
esthétique.


Faire pousser dans la maison propre
===================================

Si on compte bien, il y a bien plus de sept éléments.

Pour tout dire, nous en avons recensé vingt-trois. Alors que les Iraniens
non zoroastriens y placent au moins sept objets, dont le nom commence
par un « s », les zoroastriens choisissent au moins sept objets qui
renvoient aux divinités zoroastriennes.

Zoroastriennes ou pas, les tables accueillent toujours les fameuses pousses – sabzeh.
Ces dernières, en plus de symboliser le renouveau de la végétation, ont
une fonction propitiatoire : à la fin des festivités, les femmes doivent
jeter les pousses dans l'eau, rituel qui permet de souhaiter un mariage
dans l'année pour les jeunes filles.

Ces gestes font écho à la pratique du ménage de printemps opéré juste
avant Norouz, où l'on met tout en ordre pour accueillir les âmes des
morts qui reviennent chez eux à cette occasion et repartent après les
cérémonies qui se clôturent par un pique-nique.

Notons toute l'ambivalence d'un rite qui consiste à faire pousser dans
la maison propre des graines prometteuses d'un renouveau favorable, mais
également porteuses de mauvaises choses – c'est pourquoi on s'en débarrasse.


Le plus étonnant est que cette pratique est bien plus répandue qu'on ne
le pense : on la retrouve dans le monde chrétien, chez les hindous.
Et paradoxalement, elle ne se trouve pas là où elle devrait se trouver,
chez les zoroastriens parsis d'Inde, alors qu'en diaspora on a tendance
à conserver les traditions.

L'élément symbolique de la graine germée est l'objet d'une vaste enquête
menée par l'anthropologue Salvatore D'Onofrio (1), qui met en lumière
sa présence à travers le culte d'Osiris en Égypte ancienne, la Sainte-Barbe
dans le Liban maronite actuel, les Pâques catholiques en Calabre, Sardaigne,
Sicile, et jusqu'au Noël provençal (avec les treize desserts).

Rejetant l'hypothèse diffusionniste qui présuppose l'existence de « foyers culturels »
limités pour expliquer qu'un trait culturel soit présent dans différentes
régions du monde, l'auteur met en avant qu'une pratique similaire n'est
pas le vestige d'une structure ancienne, et montre comment des cultures
différentes peuvent exprimer des choses similaires.


S'emparer du pouvoir physiologique des femmes
=================================================

En Italie, lors de la semaine sainte en Calabre, les pousses accompagnent
à Nocera Terinese des rites de sang : des hommes s'imposent des blessures
précises qui provoquent des effusions de sang, devant des images de la
Vierge, devant certaines maisons que le flagellant marque de son sang
(à commencer par sa propre maison) et devant les pousses.

Ces pratiques rituelles mettent en avant une inversion des sexes : les
hommes, revêtus d'attributs féminins, cherchent à s'emparer du pouvoir
physiologique des femmes, et les femmes, en cultivant des germes éphémères,
endossent le rôle des hommes, traditionnellement affectés aux travaux
des champs.

Inversion des sexes, renversement d'un ordre, transgression, le temps
d'un rituel.

Allusion à l'univers incestueux du Norouz persan des Sassanides (224-642) :
leurs mages, animés par une vision eschatologique de la société, pensaient
que la fin des temps approchait et qu'il fallait répéter les élites.

Ahura Mazda avait épousé sa fille et les prêtres ont considéré que tout
le monde devait respecter ces formes de mariage qui sous-tendent le
terrestre et le céleste, et qui étaient censées apporter la chance et
l'exemption de toutes les fautes.

En 642, l'empire sassanide s'effondre… la conquête arabe importe l'islam.

Mais la fête de Norouz, inscrite en 2019 au patrimoine immatériel de l'Unesco,
s'est transmise et elle est perpétuée au sein des familles et grâce au
réseau intracommunautaires.

**Une fête où le rire et la joie sont étroitement associés à l'ordre divin,
deux choses à cultiver par les temps qui courent**.





