.. index::
   pair: Conférence; 2023-03-13

.. _conference_iran_2023_03_13:

======================================================================================
2023-03-13 **La révolution des femmes et de la rue peut-elle aboutir ?** à 20h30
======================================================================================

- https://www.ctm-grenoble.org/actualite/forum-de-levenement-lundi-23-janvier/
- http://www.sciencespo-grenoble.fr/membres/burdy-jean-paul/

Iran: la révolution des femmes et de la rue peut-elle aboutir ?
==================================================================

- Une révolution et sa répression : état des lieux.
- Les causes de cette révolution.
- Y a-t-il un espoir que la révolution gagne ?


.. raw:: html

   <iframe width="900" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
   src="https://www.openstreetmap.org/export/embed.html?bbox=5.750141143798829%2C45.201022583791456%2C5.764303207397461%2C45.20722125670528&amp;layer=mapnik"
   style="border: 1px solid black">
   </iframe>
   <br/><small><a href="https://www.openstreetmap.org/#map=17/45.20412/5.75722">Afficher une carte plus grande</a></small>



Centre théologique de Meylan, 15 chemin de la Carronnerie 38240 Meylan
à 20h30

Avec Zoreh Baharmast, présidente de la Ligue des Droits de l'Homme Grenoble-Métropole
et Jean-Paul Burdy, maître de conférences honoraire en histoire à Sciences Po Grenoble

sur place au Centre Théologique de Meylan
en visio-conférence par Zoom
ID de réunion : 810 7835 9852 – Code secret : 170331


.. figure:: images/affiche_la_revolution_des_femmes_et_de_la_rue_peut-elle_aboutir.png
   :align: center

