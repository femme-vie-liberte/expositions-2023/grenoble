
.. _annonce_gre_mag_2023_03_04:

=================================================================================================================
Samedi 4 mars 2023 **Article sur le magazine de la ville de grenoble numero 42 avril-mars 2023**
=================================================================================================================


.. figure:: images/marjane_ladan_zarah.png
   :align: center


L'introduction est fausse : Non, ce n’est pas la CISEM qui a organisé les premières manifestations, mais les organisations iraniennes de Grenoble
=====================================================================================================================================================

.. warning:: Non, ce n'est pas la CISEM qui a organisé les premières
   manifestations.


.. figure:: images/premiere_manifestation_marjane.png
   :align: center

   Signé: LDH Grenoble Metropole (Iran concrètement)


- :ref:`iran_luttes:iran_grenoble_2022_09_24`


Une exposition en trois volets
===================================

Si le soutien politique et social est nécessaire, les actions culturelles
sont également très importantes afin de faire passer un message.

Mardi 7 mars 2023, la Maison de l'international proposera ainsi une exposition
conçue par Zoya Vergain, membre d'Iran Solidarités.

L'exposition explorera le mouvement des femmes et notamment l'histoire
du féminisme en Iran, les répressions islamistes perpétrées depuis la
révolution de 1979 jusqu'au meutre de Mahsa Jina Amini, et enfin les événements
au sein du pays depuis le 16 septembre 2022.

Cette dernière chronologie sera racontée à travers des tableaux, des textes
ou encore des céramiques, issus du travail d'artistes iraniens installés
à Grenoble.



Dernières manifestations à Grenoble
=======================================

- :ref:`iran_grenoble_2023_02_25`
- :ref:`iran_grenoble_2023_02_18`
- :ref:`iran_grenoble_2023_02_11`
- :ref:`iran_grenoble_2023_02_04`
- :ref:`iran_grenoble_2023_01_28`
- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`
