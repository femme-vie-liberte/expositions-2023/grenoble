
.. index::
   ! Vernissage

.. _vernissage:

=========================================================================================================
**Vernissage le mardi 7 mars 2023 à 18h30**
=========================================================================================================

.. figure:: images/invitation.png
   :align: center


Quelques photos
====================

.. figure:: images/concert.png
   :align: center


.. figure:: images/salle_3.png
   :align: center

.. figure:: images/salle_3_numero_2.png
   :align: center


.. figure:: images/salle_4.png
   :align: center

.. figure:: images/photo_de_groupe_1.png
   :align: center

.. figure:: images/photo_de_groupe_2.png
   :align: center

.. figure:: images/photo_de_groupe_3.png
   :align: center

.. figure:: images/photo_de_groupe_4.png
   :align: center


Discours
==========


.. _discours_zoya_2023_03_07:

Discours de Zoya lors de l'ouverture de l'exposition "Iran Femme Vie Liberté" à la Maison de l'International
---------------------------------------------------------------------------------------------------------------

L'idée de cette exposition est venu d'un constat simple. Tout le monde
parlait de la mort de Mahsa Jina Amini sans que personne ne sache vraiment
ce qui se passait en Iran.

Certains ont même pensé que ce mouvement était anti foulard or la protestation
des femmes en Iran était contre le voile obligatoire pour lequel elles peuvent
mourir.
Beaucoup d'amis ont répondu présent à cette proposition et chacun a travaillé
avec une motivation d'enfer pour faire connaître la situation en Iran.

Les thèmes principaux ont été choisis en groupe:

- **le féminisme en Iran** (:ref:`salle 1 <salle_1_grenoble>`)
- **histoire de la répression depuis la révolution de 1979** (:ref:`salle 2 <salle_2_grenoble>`)
- **la révolution en cours depuis le 16 septembre 2022** (:ref:`salle 3 <salle_3_grenoble>`)
- **Une quatrième partie pour exposer des œuvres d'artistes iraniens en
  relations avec le thème de l'exposition** (:ref:`salle 4 <salle_4_grenoble>`)

On se retrouve aujourd'hui, jour pour jour, au 44eme anniversaire de la
première annonce du voile obligatoire en Iran.

Le 7 mars 1979, les journaux ont publié les paroles de khomeini, le guide
religieux à l'époque, "les femmes doivent aller au bureau en foulard".
A l'époque, c'était inimaginable pour nous toutes.

Le lendemain, à l'occasion de la journée du droit des femmes, il y a eu
une manifestation gigantesque de femmes,  pas suffisamment soutenue par
les hommes et même les intellectuels.
La question des femmes semblait dérisoire par rapport aux autres problèmes
de la société. Heureusement qu'il y a eu dew changements depuis.

Le hasard fait qu'on se trouve ici aujourd'hui même, le 7 mars pour
inaugurer cette exposition sur une révolution avec les femmes en tête
de cortège et avec le slogan femme vie liberté après la mort de Mahsa
Jina Amini pour un voile non ajusté.

Un mouvement qui  témoigne du soulèvement des femmes en Iran, de leur
désir de liberté. La révolte des femmes, soutenu par les hommes et toutes
les couches de la société cette fois-ci, s'est répandu par tout et suivi
par tous. Malgré les morts et les arrestations, les tortures, les viols
de femmes, d'hommes et d'adolescents, ce mouvement a continué son chemin.

Et même si après 4 exécutions, les manifestations ont perdu de vitesse,
il ne faut pas se tromper. Le mouvement n'est pas mort. Loin de là.
C’est juste une acalmi avant la tempête.
Ce mouvement  s'organise et réfléchit. Rien est revenu à la normale, les
manifestations et les greves continuent. En ce moment même, le régime est
en train de se venger des filles en les intoxicant aux gaz dans les écoles,
pour les empêcher d'aller a l'école.  On est déjà à 2 morts.

Dans les prisons les pires tortures continuent. Ils continuent à forcer
les aveux de prisonniers sous la torture pour les condamner à mort.
La situation économique est dramatique avec près 60% de la population
sous le seuil de pauvreté.

Loin de l'Iran, nous voulons être la voix de notre peuple et demander à
la communauté internationale de mettre le sepah pasdarans, la force de
répression du régime iranien, sur la liste des organisations terroristes,
de faire pression pour l'arrêt immédiat des condamnations à mort, la
libération des prisonniers politiques, et l'expulsion des ambassadeurs
iraniens.

De reconnaître le droit du peuple iranien à la liberté, l'arrêt immédiat
de l'intoxication des filles dans les écoles.

La répression du désir de liberté du peuple iranien, a fait plus de 500
morts et plus de 20 000 emprisonnements.

Femme Vie Liberté !

- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

