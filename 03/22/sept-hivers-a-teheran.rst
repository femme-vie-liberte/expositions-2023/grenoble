.. index::
   pair: Film ; Sept hivers à Téhéran


.. _film_2023_03_22:

===============================================================================================================================
**Sept hivers à Téhéran, avant-première le mercredi 22 mars 2023 à 20h20**
===============================================================================================================================

- https://cinemalemelies.org/-sept-hivers-

Annonce
=========

.. figure:: images/affiche.png
   :align: center

   Sept hivers à Téhéran

Sept hivers à Téhéran
de Steffi Niederzoll
Avant-première le mercredi 22 mars 2023 à 20h20

Séance en présence de la réalisatrice Steffi Niederzoll

en partenariat avec la Ligue pour la défense des droits de l'homme en Iran
et Amnesty International


En 2007 à Téhéran, Reyhaneh Jabbari, 19 ans, poignarde l’homme sur le
point de la violer. Elle est accusée de meurtre et condamnée à mort.

A partir d’images filmées clandestinement, Sept hivers à Téhéran montre
le combat de la famille pour tenter de sauver Reyhaneh, devenue symbole
de la lutte pour les droits des femmes en Iran.

“Un choc, d'une puissance rare” Jérôme Garcin - L'Obs


- https://twitter.com/femmeazadi/status/1635619237795438592#m

Un film poignant à ne pas manquer auquel @femmeazadi s'associe.

Dans toutes les salles de ciné le 29 mars! Cest l'histoire de Reyhaneh Jabbari
victime des dysfonctionnements misogynes du système judiciaire en Iran.

@nourfilms_ @berlinale #AmnestyInternational
#StopExecutionsInIran
